﻿ptvFinder is an application intended to accept a lat/lon and transport type, and output upcoming public transport departures at nearby stops. 

Uses the PTV API 2.2.0
https://www.data.vic.gov.au/data/dataset/ptv-timetable-api

Your config.h file should include the following definitions:
#define DEVID <yourdevid>
#define SECKEY <yourseckey>

These can be obtained by sending an email to APIKeyRequest@ptv.vic.gov.au with the following subject line:
"PTV Timetable API - request for key"
(Seriously though, just read that first link. It explains all of it)
