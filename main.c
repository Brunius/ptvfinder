/*--------------------------------------------------------
ptvFinder does things. Go read the readme.txt, jesus
--------------------------------------------------------*/


#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <hmac_sha1.c>

#define BASEURL "http://timetableapi.ptv.vic.gov.au"

struct stopdata {
	int type;
	char suburb[64];
	int id;
	char name[256];
	float lat;
	float lon;
	float distance;
};

struct servicedata {
	int type;
	int id;
	char destination[256];
};

void HMAC_process (char * query[]) {
	/*--------------------------------------------------------
	Feed query into HMACSHA1
	Get response back and add to end of query with &signature=hash
	--------------------------------------------------------*/
	char hash[21];
}

void nearme(double lat, double lon, struct stopdata * stopArray[]) {
	/*--------------------------------------------------------
	The 'Stops Nearby' query is on page 31 of 13 APR 2016 PDF.
	This function finds a number of nearby stops equal to
	the size ofstopArray (ie it will fill the array).
	
	Stops are arranged by distance, with the lowest distance
	being 0, next nearest 1, and so on.
	
	The maximum number of results is 30. This is due to an API
	limitation.
	--------------------------------------------------------*/
	const char nearQueryBlank[150] = "/v2/nearme/latitude/%f/longitude/%f?devid=%s";	//Define query
	char query[2000];
	sprintf(query, nearQueryBlank, lat, lon, DEVID);
	//query is defined. Now we need to get the signature through HMAC-SHA1.
	
	
	printf("%s\n", query);
}

int main(int argc, char *argv[]) {
	if (argc != 4) {
		//If incorrect number of parameters, exit with -1 and describe required parameters
		printf("usage: %s latitude longitude type\naccepted types: train, bus, tram, other\n", argv[0]);
		return -1;
	} else {
		double latitude = atof(argv[1]);
		double longitude = atof(argv[2]);
		char type[6];
		strcpy(type, argv[3]);
		
		//All parameters defined! Onward, to glory!
		
		printf("%f, %f, %s\n", latitude, longitude, type);
		nearme(latitude, longitude, NULL);
	}
}

//Read in arguments
//Find nearest POI for given transport type
//Request next trips for that POI
//Display